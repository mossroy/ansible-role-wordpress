# Ansible role Wordpress

Simple Ansible role to install and configure Wordpress in my self-hosting context

The `wordpress_mysql_password` variable needs to be set.

The following optional variables can be overriden (defaulting to "wordpress") :
*  `wordpress_directory_name`
*  `wordpress_mysql_database_name`
*  `wordpress_mysql_username`
*  `wordpress_apache_site_name`
*  `wordpress_apache_site_alias`
 
It also expects that there is a DNS alias `wordpress_directory_name` that points to the currently active server, and that the frontal reverse-proxy uses this DNS alias.